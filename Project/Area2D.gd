extends Area2D

onready var flagPacked = preload("res://Scenes/Dead_Body.tscn")

var flagPrev = self.get_position_in_parent()

var newPos = Vector2(496, -32)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		get_tree().reload_current_scene()
		var dead_Body = flagPacked.instance()
		dead_Body.position = newPos
		get_tree().get_root().add_child(dead_Body)
