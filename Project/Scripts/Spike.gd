extends Sprite

onready var flagPacked = preload("res://Scenes/Dead_Body.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Spike_body_entered(body):
	get_tree().reload_current_scene()
	var dead_Body = flagPacked.instance()
	add_child_below_node(dead_Body,get_tree().Level_1)